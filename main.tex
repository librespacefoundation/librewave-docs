\documentclass{report}
\usepackage[a4paper, total={7.3in, 10.7in}]{geometry}
\usepackage{graphicx} % Required for inserting images
\usepackage[toc,acronyms,section,nopostdot,nonumberlist,section=section,numberedsection]{glossaries}
\usepackage[hidelinks]{hyperref}
\usepackage{cleveref}
\usepackage{xcolor, soul}

\setcounter{secnumdepth}{4}
\setcounter{tocdepth}{4}

\newcommand\TBD[1]{\textcolor{red}{#1}}

\title{LibreWave ECP5 SoM Design Document}
\author{Libre Space Foundation }
\date{August 2023}

\makeglossaries{}

%% Acronyms
\input{glossaries/acronyms}

\begin{document}

\maketitle

\section*{Introduction}
This document is purposed to serve as a description of the design process followed in the LibreWave ECP5 SoM, as a justification for the various design choices as well as a guide on FPGA hardware design for intermediately - skilled hardware designers.
More than a few are the parts where this document borrows information from the manufacturer-supplied documentation, yet information here is provided in a more summarized and utilitarian manner and with more focus to the hardware design aspects.

\newpage
\printglossary[type=\acronymtype]

\chapter{Schematic Design}
\section{Schematic structure}
As is the case with high pin count or high component count designs, the schematic is broken down into hierarchical sheets.
In this section the breakdown of the schematic is explained in order to serve as a quick reference for someone that studies it for the first time.
Additionally, some ECAD--specific tools that are used are explained.

\subsubsection{KiCAD Buses}
The large number of signals that need to be connected between components and sheets necessitates the use of compact schematic implementation that is easy to visualize.
KiCAD buses serve this purpose quite well.
In this design, several bus aliases have been defined in order to shorten the labels present on the bus lines.
To define a bus alias in KiCAD 7, from the schematic editor navigate into File $>$ Schematic Setup $>$ Bus Alias Definitions.
Another helpful characteristic of buses, which is exploited in this design is the ability to use hierarchical labels.
One can simply connect a hierarchical label to the bus line as if it was a simple signal label, transfer the bus signals between sheets using a bus line and then unfold the bus in the destination sheet.
The hierarchical label must follow the bus label naming rules and using a bus alias is allowed.

\TBD{Add a 3-part picture showing 2 hierarchical sheets with unfolded buses at the left and right part and the bus on the top sheet on the center part}

\section{Mezzanine connector pinout}
Provided below is a table with pin names and their functions.
This table serves explanatory purposes regarding the pin functions and names.
\textbf{Although this document is meant to be updated regularly, always check the latest release on GitLab before transferring the pinout to another board, or using a version of the LibreWave ECP5 SoM}.

\TBD{Add said table}

\section{FPGA Configuration}
\subsection{JTAG}
A 4k7 resistor network is added to pull some pins high and some others low.
In particular, JTDO, JTDI, JTMS need to be pulled high, while JTCK needs to be pulled low according to \cite{ECP5-hw-checklist}.

\subsection{Configuration -- Boot mode}
By "configuration procedure", the loading of the configuration SRAM from a non-volatile memory is implied.
The ECP5 FGPA provides a set of pins named the sysCONFIG which can be used to implement several modes for the configuration procedure, namely : Master SPI, Slave SPI, Slave Parallel, Slave Serial, JTAG.
The sysCONFIG pins are powered by the VCCIO8 rail, while the configuration modes are described in more detail in \cite{ECP5-sysCONFIG}.

\subsubsection{Deviations from the AC7Z020 pins}
On the LibreWave ECP5 SoM, the 3 configuration pins (CFGMDN) are broken out to the mezzanine connectors.
However, placeholders for a resistor configuration has also been employed on-board in case the client/user has a priori knowledge of the desired boot mode and wants to consolidate the choice on the SoM.
Also, the PROGRAMN and INITN pins are broken out to the mezzanine connectors.

\subsubsection{CFGMDN}
Besides JTAG, the other modes require a specific HIGH/LOW combination on the CFGMDN[2:0] pins : 
\begin{itemize}
    \item Master SPI (MSPI) : CFGMDN[2:0] = [010]
    \item Slave SPI (SSPI) CFGMDN[2:0] = [001]
    \item Slave Serial CFGMDN[2:0] = [101]
    \item Slave Parallel CFGMDN[2:0] = [111]
\end{itemize}
In this design, besides JTAG, the Master SPI mode is employed.
Regarding pull-up/down values, $<$ 500R pull-down and 4k7 pull-up resistor values are suggested.
In practice, pull-down values larger then 500R (e.g. 1k, 4k7) have been used.

\subsubsection{PROGRAMN}
The PROGRAMN pin, is used (among others) to exit user mode and put the FPGA into configuration mode. This pin is exposed on the mezzanine connectors.

\subsubsection{INITN}
The INITN is an active low pin, which is asserted after a configuration "command" (Power on, PROGRAMN falling edge, or IEEE 1532  REFRESH command) for some time and when it is de-asserted, it indicates that the FPGA is ready for the configuration bits.
Probably the most useful feature of the INITN pin in terms of hardware design is that any assertions besides the initial assertion indicate an error in the configuration process.

\subsubsection{DONE}
The DONE pin is an open drain pin which signals that the FPGA is in user mode.
By keeping the pin asserted (LOW), the user can prohibit the FPGA from entering user mode (for example if the user needs to synchronize the start of many FPGAs with different configuration times).

\subsubsection{MCLK -- CCLK}
The CCLK pin is a clock input used for the Slave SPI mode, so no further reference to it is made in this document.
The MCLK is a clock output used for the Master SPI mode.
It default frequency is 2.4 MHz, but it can be configured to any of the following values : \{2.4, 4.8, 9.7, 19.4, 38.8, 62\} MHz.
The pin tristates when the FPGA enters user mode.
A 1k pull-up resistor should be present on the MCLK line.

\subsubsection{HOLDN/DI/BUSY/CSSPIN}
The names mentioned in the title all refer to the same pin and for the selected configuration mode (MSPI), the appriate name is CSSPIN.
This pin acts as an active low Chip Select pin for the SPI protocol.
It is recommended to add a pull-up resistor with a value between 4k7 and 10k.
This pin can either be used as a generic IO or as a Chip Select for the flash memory after the FPGA has entered user mode, if configured appropriately.

\subsubsection{WRITEN}
This pin is mainly used for the Slave Parallel mode, but also has an effect on the Master SPI mode, thus a short mention is made here.
When this pin is pulled LOW, it results in a 0XFF byte to be written on MOSI at the start of the configuration, which is intnded to cause a forced reset in quad-SPI devices.
In case WRITEN is pulled HIGH, the Master SPI starts configuration without writing the 0XFF byte.

\subsubsection{D0/MOSI}
In Master SPI configuration mode, the MOSI pin drives control and data bytes to the flash memory at the falling edge of the MCLK clock.
A 10k pull-up resistor should be present on the line.
The pin's functionality as MOSI can persist when the FPGA has entered user mode, it configured appropriately.

\subsubsection{D1/MISO}
In Master SPI configuration mode, the MISO pin serves as data input to the FPGA.
A 10k pull-up resistor should be present on the line.
Similarly to the MOSI pin, its functionality can persist in user mode, if configured appropriately.

\subsubsection{IO[3:0]}
These pins are used in the case a Dual or Quad-SPI flash memory is employed in the design.
For Quad-SPI, 10k pull-up resistors should be present on the IO3 and IO2 lines.

\section{Non-volatile memory selection}
An external non-volatile memory is used to store the FPGA configuration program.
This device is a flash IC which connects to the FPGA via single, dual or quad SPI -- in this design the quad SPI mode is selected.
From the supported models list in Lattice Diamond, the S25FL256SAGNFV000 flash IC was chosen because of its small footprint and on--chip \acrshort{ecc}, while its NOR technology has been claimed in the literature to be preferred over NAND in terms or radiation resilience \cite{SD-radiation}. 
According to \cite{ECP5-sysCONFIG} the maximum capacity required for any ECP5 model even with dual write (nominal plus fail-safe configuration stored in flash) is 64 Mbit, so the 256 Mbits of capacity in the selected model are enough.

\section{FPGA Power and Decoupling}
\subsection{Power supply voltage levels}
The power supply levels needed for the ECP5 FGPAs are descibed in \cite{ECP5-hw-checklist} while some specific values are provided in \cite{ECP5-datasheet}.
The specific voltage levels are listed below :
\begin{itemize}
    \item \textbf{VCC} : The core supply voltage. For the ECP5 (not the 5G model), it is required that $1.045 V < VCC < 1.155 V$. A nominal value of 1.1V is chosen.
    \item \textbf{VCCAUX} : An auxiliary power supply. For both the ECP5 and -5G it is required that $2.375 V < VCCAUX < 2.625 V$. A nominal value of 2.5V is chosen.
    \item \textbf{VCCIO[0-3,6-8]} : An I/O power supply. It is generally required that $1.2 V < VCCIO < 3.3 V$, with individual bank voltages being selected as per user needs. On LibreWave ECP5 SoM, some voltage levels are enforced by the expected peripheral voltage levels on SatNOGS COMMS (e.g. the EMMC bank voltage must be 1.8V). Moreover, the VCCIO8 bank voltage is used for the configuration pins and for the JTAG peripheral. Taking the aforementioned constraints into account, the following nominal supply voltages are selected per bank, with a 5\% tolerance applied on the nominal voltage level : 
        \begin{itemize}
            \item VCCIO8 : A nominal voltage level of 3.3V is selected for easy compatibility with the flash and JTAG.
            \item VCCIO7 : A nominal voltage level of 2.5V is selected because this bank will interface with the LVDS signals. Bank 7 is chosen to interface with the LVDS signals because it is the one containing the DSP slices, which will be used to process the data flowing in/out via LVDS.
            \item VCCIO6 : A nominal voltage level of 3.3V is selected. This bank will be free of peripherals to allow one to utilize the block RAM of the FPGA. The 3.3V nominal value was selected for convenience and is not required specifically for the block RAM.
            \item VCCIO2 \& VCCIO3 : A nominal voltage of 1.35V is selected in order to be able to interface with DDR3L ICs.
            \item VCCIO1 : A nominal voltage of 1.8V is selected in order to interface with the eMMC and the RGMII protocols in the voltage levels expected by SatNOGS COMMS (and used by the AC7Z020).
            \item VCCIO0 : A nominal voltage of 3.3V is selected in order to interface with common peripherals on the host board.
        \end{itemize}
    \item \textbf{VCCA} : The analog    power supply for the SerDes peripheral. For the ECP5UM (not the 5G model), it is required that $1.045 V < VCCA < 1.155 V$. A nominal value of 1V is chosen. It is important that this rail is isolated from the other 1.1V power rails (for example, the VCC power rail). To achieve isolation, \cite{decoupling-bypass-FPGA} suggests to use a 4 uH to 10 uH ferrite bead with low resistance such as the Murata BLM31B601S and BLM11B601SPB.
    \item \textbf{VCCAUXA} : The auxiliary SerDes supply voltage. For both the ECP5UM and the -5G it is required that $2.374 V < VCCAUXA < 2.625 V$. A nominal value of 2.5V is chosen.
    \item \textbf{VCCHRX} : The termination voltage for the SerDes input buffer. For the ECP5UM (not the 5G model), it is required that $0.30 V < VCCHRX < 1.155 V$. A nominal value of 1.1V is chosen for the ECP5UM (not the 5G model) as per \cite{serdes-electrical}.
    \item \textbf{VCCHTX} : The termination voltage for the SerDes output buffer. For the ECP5UM (not the 5G model), it is required that $1.045 V < VCCHTX < 1.155 V$. A nominal value of 1.1V is chosen for the ECP5UM (not the 5G model) as per \cite{serdes-electrical}.
\end{itemize}

\subsection{Decoupling}
Power decoupling and bypass filtering is described in the homonymous reference \cite{decoupling-bypass-FPGA}.
The guidelines provided in the reference with respect to schematic design state that a 0.1 uF or 0.01 uF capacitor must be placed per power pin.
However, this is not possible for all rails and it is also not always needed.
For example, the VCC power pins on a particular model package form  a 4x4mm square in the center of the BGA package with 0.8mm pitch.
For this model, there are designs that use 6 or 10 capacitors, while in theory 20 would be needed.
Moreover, the guidelines state that bulk capacitors in the order of 1 uF to 10 uF must be distributed near the FPGA, but not particularly close to the power pins.
Finally, a large (100 uF) electrolytic capacitor is recommended at the power source, but this is not specific to the FPGA design - large value, low-ESR capacitors are placed on the output stage of \acrshort{smps} anyways.
In this design, decoupling capacitors were utilized as described in Table \ref{tab:fpga-decoupling}. 

\begin{table}[h!]
\begin{center}
\begin{tabular}{|l|l|l|l|}
\hline
\multicolumn{1}{|c|}{\textbf{Power Rail}}  & \multicolumn{1}{c|}{\textbf{10 nF}}     & \multicolumn{1}{c|}{\textbf{100 nF}}   & \multicolumn{1}{c|}{\textbf{10 uF}} \\ \hline
\multicolumn{1}{|l|}{VCC}                  & \multicolumn{1}{l|}{4x 0402 X7R}        & \multicolumn{1}{l|}{10x 0402 X7S}      & 2x 0603 X7R                         \\ \hline
\multicolumn{1}{|l|}{VCCAUX}               & \multicolumn{1}{l|}{2x 0402 X7R}        & \multicolumn{1}{l|}{4x 0402 X7S}       & 2x 0603 X7R                         \\ \hline
\multicolumn{1}{|l|}{VCCHTX}               & \multicolumn{1}{l|}{2x 0402 X7R}        & \multicolumn{1}{l|}{4x 0402 X7S}       & 2x 0603 X7R                         \\ \hline
\multicolumn{1}{|l|}{VCCHRX}               & \multicolumn{1}{l|}{2x 0402 X7R}        & \multicolumn{1}{l|}{4x 0402 X7S}       & 2x 0603 X7R                         \\ \hline
\multicolumn{1}{|l|}{VCCA}                 & \multicolumn{1}{l|}{2x 0402 X7R}        & \multicolumn{1}{l|}{4x 0402 X7S}       & 2x 0603 X7R                         \\ \hline
\multicolumn{1}{|l|}{VCCAUXA}              & \multicolumn{1}{l|}{2x 0402 X7R}        & \multicolumn{1}{l|}{4x 0402 X7S}       & 2x 0603 X7R                         \\ \hline
\multicolumn{1}{|l|}{Per bank}             & \multicolumn{1}{l|}{None}               & \multicolumn{1}{l|}{2x 0402 X7S}       & 1x 0603 X7R                         \\ \hline
\multicolumn{4}{|l|}{}                                                                                                                                              \\ \hline
\multicolumn{1}{|c|}{\textbf{Part Number}} & \multicolumn{1}{l|}{GRM155R71H103JA88D} & \multicolumn{1}{l|}{GRM155C71H104JE19} & GRM188Z71A106KA73D                  \\ \hline

\end{tabular}
\caption{FPGA decoupling capacitors}
\label{tab:fpga-decoupling}
\end{center}
\end{table}

The selected package for the 10 nF and 100 nF capacitors was 0402 (imperial), due to its higher resonant frequency and compact size (in comparison to 0603).
For the bulk capacitors, 0603 package was used to reduce the effect of DC bias.
To maintain the ability of semi-manual assembly (for example manual pick and place machine), 0201 components were not used.
The choice of X7S dielectric for the 100 nF capacitors might seem peculiar, so it is noted here that the X7S dielectric was chosen because the selected capacitor manufacturer (Murata) offered a X7S model with better tolerance (5\% vs 10\%) and same temperature derating characteristics as its X7R models.
The temperature derating curves for the 10 nF and 100 nF capacitors are provided below for a 3.3V DC bias, while only the DC bias derating curve could be found for the 10 uF capacitor.

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/FPGA_Decoupling_C/GRM155R71H103JA88_3V3.png}
  \caption{GRM155R71H103JA88D DC bias \& temperature derating}
  \label{fig:GRM155R71H103JA88D}
\end{figure}

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/FPGA_Decoupling_C/GRM155C71H104JE19_3V3.png}
  \caption{GRM155C71H104JE19 DC bias \& temperature derating}
  \label{fig:GRM155C71H104JE19}
\end{figure}

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/FPGA_Decoupling_C/GRM188Z71A106KA73D_C_DC_bias.png}
  \caption{GRM188Z71A106KA73D DC bias \& temperature derating}
  \label{fig:GRM188Z71A106KA73D}
\end{figure}

\section{Switching Mode Power Supplies (SMPS)}
To provide power at the aforementioned voltage levels, a 5V and a 3.3V power supply rail are expected to be provided by the motherboard and several \acrshort{vrm}s are employed on-board.
For the on-board VRMs, the TPS62913 DC-DC converter is chosen since it is also used in SatNOGS COMMS and thus is a proven design and the design team has experience with it.
The problem with the circuit configurations proposed by the manufacturer and with the one that is used in SatNOGS COMMS is that they are too big for this application.
In the following paragraphs the engineering process followed to achieve a smaller, yet functional and safe, design is described.

\subsection{Output capacitors}
The 3x 22uF 0805 capacitors used in SatNOGS COMMS is just one possible implementation of the output capacitance requirement, but not the only one possible.
In fact, the IC's datasheet mentions that the effective output capacitance of the LC filter can range from 40 uF to 200 uF.
It is very hard to create the 40 uF capacitance using 0603 X7R capacitors since those are available up to capacitance values of 10 uF.
The other options are to use either X5R capacitors or a single 0805 X7R capacitor.
Regarding the 0805 option, X7R capacitors go up to 22 uF, so including DC bias, 3 of them are needed and the design will still be close to the minimum limit.
If we use X5R capacitors, we can find 47 uF capacitors in 0603 package and use 2 of them or a single 100uF in 0805 package.
After investigation, the option to use a single X5R 0805 capacitor was deemed the most viable and it is further examined.
The GRM21BR60J107ME15K in particular is a capacitor from MuRata fitting those specifications.\\

\textbf{DC bias \& Temperature derating}\\
Using MuRata's \href{https://ds.murata.co.jp/simsurfing/mlcc.html?lcid=en-us&jis=false&md5=ff15ee0accd1424775961bdb536472d6}{SimSurfing tool} one can obtain the DC bias curve of the capacitor, which is shown below.
Figure \ref{fig:GRM21BR60J107ME15K} shows the temperature effect on the capacitance, after DC bias de-rating has been applied.
The DC bias was considered to be 1.8V because this is the largest voltage output needed on the board.

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/GRM21BR60J107ME15K.png}
  \caption{GRM21BR60J107ME15K DC bias \& temperature derating}
  \label{fig:GRM21BR60J107ME15K}
\end{figure}

\textbf{Max voltage derating}\\
According to ECSS-Q-ST-30-11C-Rev1 \cite{ECSS-Q-ST-30-11C-Rev1}, the load ratio for ceramic capacitors must be 60\% up to a temperature of 85C.
The load ratio is defined as the permissible operating level after derating has been applied; given as a percentage of a parameter rating.
Considering that the max voltage of interest in our case is 1.8V, the selected max voltage rating must be at least 3V.
Ceramic capacitors rated at 4V and 6.3V are readily available, while the GRM21BR60J107ME15K is rated at 6.3V.

\subsection{Input capacitors}
A minimum \textbf{effective} input capacitance of 5 uF is needed, with a 10 uF capacitor recommended in the datasheet.
Such a capacitor can be found in a 0603 package, with 0805 packages providing better ratings.
Also, a smaller bypass capacitor of 2.2 nF is suggested in the datasheet, for which options in 0402 packages are readily available.
After investigation, the option to use a single X5R 0805 capacitor was deemed the most valuable and it it further examined.
The GRM21BR61A226ME51L in particular is a capacitor from MuRata fitting those specifications.\\

\textbf{DC bias \& Temperature derating}\\
After applying a 5V DC bias, the capacitance change with respect to temperature is shown in Figure \ref{fig:GRM21BR61A226ME51L}, which was exported from MuRata's SimSurfing tool.

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/GRM21BR61A226ME51L.png}
  \caption{GRM21BR61A226ME51L DC bias \& temperature derating}
  \label{fig:GRM21BR61A226ME51L}
\end{figure}

\textbf{Max voltage derating}\\
According to ECSS-Q-ST-11C and the process described for the output capacitors, a max voltage of 8.3V is required.
The GRM21BR61A226ME51 is rated at 10V.

\subsection{Output inductor}
The DFE322520FD-2R2M=P2 is suggested in the datasheet.
Its inductance change and temperature rise with current are exported from MuRata's SimSurfing tool and are shown in Figure \ref{fig:DFE322520FD-2R2M=P2_Lchange} and Figure \ref{fig:DFE322520FD-2R2M=P2_Trise} respectively.
Moreover, its self-resonant frequency is above 10MHz.
The inductor is safe to use up to the DC-DC converter's upper current limit (3A).

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/DFE322520FD-2R2M=P2_Lchange.png}
  \caption{DFE322520FD-2R2M=P2 inductance vs current}
  \label{fig:DFE322520FD-2R2M=P2_Lchange}
\end{figure}

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/DFE322520FD-2R2M=P2_Trise.png}
  \caption{DFE322520FD-2R2M=P2 temperature rise vs current}
  \label{fig:DFE322520FD-2R2M=P2_Trise}
\end{figure}

\subsection{System Simulation}
Although the individual component choices were justified above, a system-wide simulation will provide a more complete proof of proper system operation.
Said simulation is performed in TI's Webench Power Designer tool.
Simulations are performed for an output voltage of 1.8V and an output voltage of 1.1V.

\subsubsection{Parameters}
To include derated values in the simulation, some custom components had to be defined.
\begin{itemize}
    \item Output capacitor : GRM21BR60J107ME15K with 48.3 uF DC bias and temperature derated capacitance at (1.8V, 85 deg C). ESR varies with frequency and a value of 10 mOhm was chosen based on the manufacturer-provided R-frequency curve.
    \item Input capacitor : GRM21BR61A226ME51L with 7.40 uF DC bias and temperature derated capacitance at (5v, 85 deg C). ESR varies with frequency and a value of 10 mOhm was chosen based on the manufacturer-provided R-frequency curve.
    \item Output inductor : DFE322520FD-2R2M=P2 with 1.8 uH current derated inductance at 3A, max DC current 5A and 46 mOhm DC resistance.
\end{itemize}

\subsubsection{Results -- Vout$=$1.8V}
To check if the system is safe to operate with the derated componets, the gain and phase margin plots are presented in Figure \ref{fig:margins_1V8_Vout}.
The gain margin is 6.8 dB and the phase margin is 75.5 deg.
The system performance in a load transient scenario is also simulated.
Since this simulation does not provide the full PDN performance, it is not a proof of adequate PDN load regulation, yet it serves as an indicator of the DC-DC converter system integrity.
Results are presented in Figure \ref{fig:load_tran_1V8_DCDC_only}.

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/margins_1V8_Vout.png}
  \caption{1.8V SMPS stability margins}
  \label{fig:margins_1V8_Vout}
\end{figure}

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/load_tran_1V8_DCDC_only.png}
  \caption{1.8V SMPS load transient}
  \label{fig:load_tran_1V8_DCDC_only}
\end{figure}

\subsubsection{Results -- Vout$=$1.1V}
To check if the system is safe to operate with the derated componets, the gain and phase margin plots are presented in Figure \ref{fig:margins_1V1_Vout}.
The gain margin is 16.87 dB and the phase margin is 59.33 deg.
The system performance in a load transient scenario is also simulated.
Since this simulation does not provide the full PDN performance, it is not a proof of adequate PDN load regulation, yet it serves as an indicator of the DC-DC converter system integrity.
Results are presented in Figure \ref{fig:load_tran_1V1_DCDC_only}.

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/margins_1V1_Vout.png}
  \caption{1.1V SMPS stability margins}
  \label{fig:margins_1V1_Vout}
\end{figure}

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/load_tran_1V1_DCDC_only.png}
  \caption{1.1V SMPS load transient}
  \label{fig:load_tran_1V1_DCDC_only}
\end{figure}

\section{ 2.5V Low Dropout Regulator (\acrshort{ldo})}
The MIC5504-2.5YM5 LDO is selected because it has been used successfully in the SIDLOC transceiver project.
Its SOT-23-5 footprint is admittedly a bit large for a high-density design like this, but not prohibitively large.

\subsection{Input \& Output capacitor selection}
For the input capacitor the datasheet states that "a 1 uF capacitor is required from the input to ground to provide stability".
For the output capacitor, the datasheet states that the IC has been optimized for use with a 1uF output capacitor, but larger output capacitor can be used as well.
According to these constraints, a capacitor with good DC bias and temperature derating characteristics is sought in order to use a single capacitor of the same model both for the input and output.
The GCM188R71E105KA64 is a 1uF, 0603, X7R capacitor from MuRata that fits the purpose.

\subsection{DC Bias \& Temperature Derating}
After applying a 3.3V DC bias (input capacitor is of greater concern since the datasheet places a "harder" stability requirement there), the capacitance change with respect to temperature is shown in Figure \ref{fig:GCM188R71E105KA64}, which was exported from MuRata's SimSurfing tool.
One can see that even in the extreme limits, capacitance stays close to the nominal value.

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/GCM188R71E105KA64.png}
  \caption{GCM188R71E105KA64 DC bias \& Temperature Derating}
  \label{fig:GCM188R71E105KA64}
\end{figure}

\subsubsection{Max voltage derating}
According to ECSS-Q-ST-11C, a max voltage of 5.5V is required. The GCM188R71E105KA64 is rated at 25V.

\section{Dual 1.8V - 3.3V Low Dropout Regulator}
To reduce reliance on power form the mezzanine connectors, a dual 1.8V -- 3.3V \acrshort{ldo} was added to the design.
Separate enable lines for the 2 outputs were among the desired features, together with fixed output levels, good accuracy, and fixed size.
The MIC5357-SGYMME offers the desired features, but imposes some constraints on the available current, due to thermal power dissipation reasons.

\subsection{Thermal considerations}
The maximum dissipated power for an ambient temperature, $T_{A}$ is given by : $$P_{D,max} = \frac{T_{J,max} - T_{A}}{\theta_{JA}}$$ 
From the datasheet, the maximum junction temperature is $T_{J,max} = 125 C$ and the die thermal resistance is $\theta_{JA} = 64.4 C/W$.
In order to set the ambient temperature, the startup temperature of other space electronics products was considered and a value of 70 C was found.
The startup condition is the most stressful because this is when the NOR flash is operational, which accounts for the largest percentage of the 3v3 channel's current.
This leads to an allowed power dissipation of : $$P_{D,max} = 0.85 W$$
This value of the allowed power dissipation results in a value of 200 mA per channel for the available current.

\subsection{Input \& Output capacitor selection}
As stated in the datasheet, an 2.2 uF capacitor is required between Vin and GND for stability. In this design, the input voltage will be 5V.
A capacitor in 0603 package with a higher value (4.7 uF) was selected due to the severe derating expected from the 5V DC-bias and the wide temperature range.
The GRM188C81C475KE11, derated at 5V, has the capacitance--temperature curve of Figure \ref{fig:GRM188C81C475KE11_5V}.
It is also rated for 16V, which adheres to the 60\% derating limit set by the ECSS standards.
Regarding the output capacitor, it is stated in the datasheet that the LDO has been optimized for a 2.2 uF output capacitor, with higher values allowed but no extra gain from using them.
To reduce BOM size, the use of the same capacitor both for the input and the output is investigated.
Derated at 3.3V and 1.8V, the GRM188C81C475KE11 has the capacitance--temperature curves of Figure \ref{fig:GRM188C81C475KE11_3V3} and Figugre \ref{fig:GRM188C81C475KE11_1V8}.

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/3V3_1V8_LDO_C/GRM188C81C475KE11_5V.png}
  \caption{GRM188C81C475KE11 DC bias \& Temperature Derating (biased at 5V)}
  \label{fig:GRM188C81C475KE11_5V}
\end{figure}

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/3V3_1V8_LDO_C/GRM188C81C475KE11_3V3.png}
  \caption{GRM188C81C475KE11 DC bias \& Temperature Derating (biased at 3.3V)}
  \label{fig:GRM188C81C475KE11_3V3}
\end{figure}

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/3V3_1V8_LDO_C/GRM188C81C475KE11_1V8.png}
  \caption{GRM188C81C475KE11 DC bias \& Temperature Derating (biased at 1.8V)}
  \label{fig:GRM188C81C475KE11_1V8}
\end{figure}

\section{Power-Up Sequence}
From the datasheet \cite{ECP5-datasheet}, one can determine the 2 conditions that must be true for the FPGA to power--up and be configured successfully.
\begin{enumerate}
    \item VCCIO8 must be powered on before at least one of VCC and VCCAUX. More specifically, the external flash must be operational before VCC or VCCAUX is powered. We can consider the NOR flash to be operational by the time it is ready to read the instruction set. This condition will be translated to a requirement for the delay value.
    \item VCCA must be powered on before VCCAUXA. No specific timing requirement is provided by the FPGA datasheet.
\end{enumerate}
The aforementioned conditions demand the following power--up sequence : $3.3V \xrightarrow{} 1.1V \xrightarrow{} 2.5V$.
Moreover, there is no issue in powering up the 1.35V and 1.8V rails together with the 1.1V rail.
Regarding the specific time delay between enablement of the various rails, the only hard requirement stems from the flash memory and demands that the delay between 3.3V and 1.1V is 300 us since this is the time needed by the IC to become available for read operations.

\subsection{Implementation}
The delay is implemented using the PG and EN pins of the SMPS and the EN pin of the LDO together with RC filters.
The relevant characteristics of those pins are provided in the paragraphs below.

The EN pin of the TPS62913 has a high-level input threshold voltage in the range [0.97,1.04] and a low-level input threshold voltage in the range [0.87,0.93].
The leakage current into the pin is at most 160nA and the internal pull-down resistor value ranges from 330 kOhm to 500 kOhm.

The TPS62913 PG pin de-assertion threshold ranges from 93\% to 98\% of the FB pin voltage, when said voltage is rising.
The PG pin is open drain and remains pulled-down until the aforementioned threshold is reached.
It requires an external pull-up resistor to produce a logic high.
As per the DC--DC converter's datasheet, the leakage current into the PG pin is max 500nA at 5V, thus it has been modeled with a 10M resistor.

The EN pin of the MIC5504-2.5YM5 registers a logic low for input voltages up to 0.2V and a logic high for input voltages higher than 1.2V.
This requires that the pull-up voltage level of the TPS62913 PG pin is higher than 1.2V -- the 3.3V rail can be used for that purpose.
There is a 4M internal pull-down resistor on this pin as per the LDO's datasheet.

For the delay between the 3.3V and 1.1V rails, a value of 6 ms is chosen to allow for component tolerances and non-modelled phenomena.
The same RC components were chosen for the delay between the 1.1V and 2.5V rails, which results in a value of 6ms for it as well. 

\subsection{Simulation}
The circuit is simulated in spice to verify the component selection.

\subsubsection{Component modelling}
The external 3.3V source is modeled with a pulsed voltage source with very short (5ns) rise time, which means that it is available immediately.
The same method is used for the PG pin.
Although it would be more correct to model the internal pull-down MOSFET of this open drain pin, it is considered that its resistance is very small (in the order of mOhm), thus not allowing the RC filter capacitor to charge.

Moreover, the leakage currents in the EN and PG pins were modeled with resistors with the values described above.

\subsubsection{Results}
The response of the 2 filters is essentially the same and it is shown in Figure \ref{fig:RC-delay-spice}, where they have been purposefully triggered at different time points to be distinguishable.
The resulting steady state value on the DC--DC converter's output is slightly lower than 3.3V due to the voltage divider formed by the filter resistor and the internal pull--down.

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.4]{pictures/RC-delay-spice.png}
  \caption{Power sequence RC delay spice model}
  \label{fig:RC-delay-spice}
\end{figure}

\section{Termination}
A comprehensive review of termination and how it affects signal quality is provided in \cite{termination-article}.
In this design no external termination resistors have been used.
To begin with, due to the small size of the board, the tracks are very small and this is probably the reason why similar designs do not include on-board termination.
Moreover, internal Thevenin termination is provided in the FPGA and driver termination and in--series resistors to delay the rising edge can be placed in the motherboard.

\chapter{PCB Design}
This chapter includes the most basic considerations regarding the design of the PCB.


\section{Controlled Impedance Traces}
The various communication protocols that have been implemented on-board need to be routed on traces with controlled impedance.
To calculate the necessary trace width for the required impedance, \href{https://www.protoexpress.com/tools/pcb-impedance-calculator/}{Sierra's Impedance Calculator}, which utilizes a 2D field solver to calculate impedance and is more accurate in comparison with other calculators that use IPC-2141 or Wadell's equations. 

\chapter{Mechanical Interface}
The form factor choice for this board was constrained by the requirement to be a drop-in compatible with the AC7Z020 Core Board from Alinx, which was used with SatNOGS COMMS at the time when the design started.
\section{Mezzannine connectors}
A pair of AXK5A2137YG mezzanine connectors are used to interface the SoM with a mother board. 
Following the naming convention found in the datasheet \cite{mezzanine-datasheet} for the P5K variant of the connector, SatNOGS COMMS has been designed with the header part of the connector (AXK6A2337YG), so the SoM was designed with the socket part.

\begin{figure}[h!]
\centering
  \includegraphics[scale=0.5]{pictures/satnogs-comms-mezzanine-header.png}
  \caption{Mezzanine headers on SatNOGS COMMS}
  \label{fig:satnogs-comms-mezzanine-header}
\end{figure}

Regarding connector placement, the board adheres to the placement found on the AC7Z020 core board.

\section{Board outline}


\bibliographystyle{IEEEtran}
\bibliography{references}

\end{document}
